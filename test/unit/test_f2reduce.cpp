#include <vector>
#include <chrono>
#include <iostream>
#include <gtest/gtest.h>
#include <m4ri/m4ri.h>
#include <cpads/random/prng.hpp>
#include "../../f2reduce.h"

inline std::vector<uint64_t> create_random_matrix(uint64_t seed, uint64_t rows, uint64_t cols, uint64_t stride, double rank_density = 1.0) {

    hh::PRNG prng(seed, 1, 2);
    uint64_t strips = (cols + 63) >> 6;
    EXPECT_LE(strips, stride);

    std::vector<uint64_t> masks(strips);

    uint64_t threshold = ((uint64_t) ((1ull << 32) * rank_density));

    for (uint64_t c = 0; c < strips; c++) {
        for (uint64_t i = 0; i < 64; i++) {
            if (prng.generate() < threshold) {
                masks[c] |= (1ull << i);
            }
        }
    }

    std::vector<uint64_t> matrix(rows * stride);
    for (uint64_t r = 0; r < rows; r++) {
        for (uint64_t c = 0; c < strips; c++) {
            uint64_t b = prng.generate64();
            b = hh::mix_circulant(b & masks[c]);
            uint64_t remcols = cols - (c << 6);
            if (remcols < 64) { b >>= (64 - remcols); }
            matrix[r*stride + c] = b;
        }
    }

    return matrix;
}

inline uint64_t check_rref(const std::vector<uint64_t>& matrix, uint64_t stride) {

    EXPECT_EQ(matrix.size() % stride, 0);
    uint64_t rows = matrix.size() / stride;

    std::vector<uint64_t> leaders(rows);
    uint64_t rank = 0;

    for (uint64_t r = 0; r < rows; r++) {
        uint64_t leader = ((uint64_t) -1);
        for (uint64_t c = 0; c < stride; c++) {
            if (matrix[r*stride+c]) {
                leader = c * 64 + hh::ctz64(matrix[r*stride+c]);
                rank += 1;
                break;
            }
        }
        leaders[r] = leader;
    }

    for (uint64_t r = 1; r < rows; r++) {
        EXPECT_LE(leaders[r-1], leaders[r]);
    }

    leaders.resize(rank);

    for (uint64_t r = 1; r < rank; r++) {
        EXPECT_LT(leaders[r-1], leaders[r]);
    }

    std::vector<uint64_t> lmask(stride);
    for (uint64_t r = 0; r < rank; r++) {
        uint64_t c = leaders[r];
        uint64_t v = matrix[r*stride+(c>>6)] & (1ull << (c&63));
        EXPECT_NE(v, 0);
        lmask[c >> 6] |= v;
    }

    for (uint64_t r = 0; r < rank; r++) {
        uint64_t lc = 0;
        for (uint64_t c = 0; c < stride; c++) {
            lc += hh::popc64(matrix[r*stride+c] & lmask[c]);
        }
        EXPECT_EQ(lc, 1);
    }

    return rank;
}


inline void timing_test(uint64_t rows, uint64_t cols, double rank_density = 1.0) {

    double f2reduce_time = 0.0;
    double m4ri_time = 0.0;

    auto start_time = std::chrono::high_resolution_clock::now();
    uint64_t rounds = 0;

    do {

        uint64_t stride = get_recommended_stride(cols);

        auto matrix = create_random_matrix((++rounds), rows, cols, stride, rank_density);
        auto matrix2 = matrix;

        {
            auto before = std::chrono::high_resolution_clock::now();
            inplace_rref_strided(&(matrix[0]), rows, cols, stride);
            auto after = std::chrono::high_resolution_clock::now();
            f2reduce_time += 1.0e-9 * std::chrono::duration_cast<std::chrono::nanoseconds>(after - before).count();
        }
        
        check_rref(matrix, stride);

        {
            mzd_t* m = mzd_init(rows, cols);

            for (uint64_t r = 0; r < rows; r++) {
                for (uint64_t c = 0; c < cols; c++) {
                    uint64_t this_bit = (matrix2[r * stride + (c >> 6)] >> (c & 63)) & 1;
                    mzd_write_bit(m, r, c, this_bit);
                }
            }

            auto before = std::chrono::high_resolution_clock::now();
            mzd_echelonize(m, 1);
            auto after = std::chrono::high_resolution_clock::now();
            m4ri_time += 1.0e-9 * std::chrono::duration_cast<std::chrono::nanoseconds>(after - before).count();

            memset(&(matrix2[0]), 0, rows * stride * sizeof(uint64_t));

            for (uint64_t r = 0; r < rows; r++) {
                for (uint64_t c = 0; c < cols; c++) {
                    uint64_t this_bit = mzd_read_bit(m, r, c);
                    matrix2[r * stride + (c >> 6)] |= (this_bit << (c & 63));
                }
            }

            mzd_free(m);
        }

        for (uint64_t i = 0; i < matrix.size(); i++) {
            EXPECT_EQ(matrix[i], matrix2[i]);
        }

    } while (std::chrono::duration_cast<std::chrono::nanoseconds>(std::chrono::high_resolution_clock::now() - start_time).count() < 100000000);

    int colour = (m4ri_time > 1.1 * f2reduce_time) ? 32 : (f2reduce_time > 1.1 * m4ri_time) ? 31 : 33;

    std::cout << "mean f2reduce time: " << (f2reduce_time / rounds) << " seconds" << std::endl;
    std::cout << "mean m4ri time: " << (m4ri_time / rounds) << " seconds" << std::endl;

    if (m4ri_time > f2reduce_time) {
        std::cout << "f2reduce is \033[" << colour << ";1m" << (m4ri_time / f2reduce_time) << "\033[0m times faster" << std::endl;
    } else {
        std::cout << "m4ri is \033[" << colour << ";1m" << (f2reduce_time / m4ri_time) << "\033[0m times faster" << std::endl;
    }
}

#define TimingTest(rows, cols) \
    TEST(F2Reduce, Strided_ ## rows ## x ## cols ## _FullRank) { timing_test(rows, cols, 1.0); } \
    TEST(F2Reduce, Strided_ ## rows ## x ## cols ## _HalfRank) { timing_test(rows, cols, 0.5); }

// odd-sized square matrices
TimingTest(1, 1);
TimingTest(3, 3);
TimingTest(5, 5);
TimingTest(7, 7);
TimingTest(9, 9);
TimingTest(27, 27);
TimingTest(81, 81);
TimingTest(243, 243);

// rectangular matrices
TimingTest(1093, 3511);
TimingTest(3511, 1093);
TimingTest(10, 1000000);
TimingTest(100, 100000);
TimingTest(1000, 10000);
TimingTest(10000, 1000);
TimingTest(100000, 100);
TimingTest(1000000, 10);

// power-of-two square matrices
TimingTest(256, 256);
TimingTest(512, 512);
TimingTest(1024, 1024);
TimingTest(2048, 2048);
TimingTest(4096, 4096);
TimingTest(8192, 8192);
TimingTest(16384, 16384);
TimingTest(32768, 32768);

